include {
  path = find_in_parent_folders()
}
terraform {
  source = "git::git@gitlab.com:t3718/public/terraform-modules/azure-container-instance-persistent.git//.?ref=v1.2.0"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=../../../../terraform.tfvars"
    ]
  }
}

dependency "aci_basic" {
  config_path = "../01-remote-desktop-aci"
  mock_outputs = {
    azurerm_resource_group_name     = "iac-rg"
    azurerm_resource_group_location = "francecentral"
  }
  mock_outputs_allowed_terraform_commands = ["init", "validate", "plan", "show"]
}

inputs = {
  azurerm_resource_group_name     = dependency.aci_basic.outputs.azurerm_resource_group_name
  azurerm_resource_group_location = dependency.aci_basic.outputs.azurerm_resource_group_location
  prefix                          = "test"
  environment                     = "test"
  pers_app_name                   = "demo-server"
  pers_ip_address_type            = "public"
  pers_os_type                    = "linux"
  pers_container_name             = "webtop"
  pers_container_image            = "lscr.io/linuxserver/webtop:ubuntu-xfce"
  pers_container_cpu              = "0.5"
  pers_container_memory           = "1.5"
  pers_container_port             = "80"
  pers_container_protocol         = "TCP"
  pers_storage_share_name         = "aci-iac-workflow-share"
}