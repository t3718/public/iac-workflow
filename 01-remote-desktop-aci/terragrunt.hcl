include {
  path = find_in_parent_folders()
}
terraform {
  source = "git::git@gitlab.com:t3718/public/terraform-modules/azure-container-instance-basic.git//.?ref=2.0.1"
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=../../../../terraform.tfvars"
    ]
  }
}

inputs = {
  prefix             = "test"
  location           = "France Central"
  environment        = "test"
  app_name           = "demo-server"
  ip_address_type    = "public"
  os_type            = "linux"
  container_name     = "webtop"
  container_image    = "lscr.io/linuxserver/webtop:ubuntu-xfce"
  container_cpu      = "0.5"
  container_memory   = "1.5"
  container_port     = "80"
  container_protocol = "TCP"
}
