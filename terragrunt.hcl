terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()
    arguments = [
      "-var-file=terraform.tfvars"
    ]
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

variable "subscription_id" {
description = "Azure subscription id" 
}
variable "client_id" {
description = "Azure client (App) id"
}
variable "client_secret" {
description = "Azure client secret (password)"
}
variable "tenant_id" {
description = "Azure tenant id"
}

provider "azurerm" {
  features {}

  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
}
EOF
}

remote_state {
  backend = "azurerm"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    storage_account_name = "iacworkflow"
    container_name       = "iac-workflow-production-tfstate"
    key                  = "${path_relative_to_include()}/terraform.tfstate"
    access_key           = "<TFSTATE_BACKEND_ACCESS_KEY>"
  }
}


