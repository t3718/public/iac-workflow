[![pipeline status](https://gitlab.com/t3718/public/iac-workflow/badges/main/pipeline.svg)](https://gitlab.com/t3718/public/iac-workflow/-/commits/main) 

# IaC workflow demo

## Architecture

![Architecture](terraform-best-practice-demo.png "Architecture")

### Infrastructure repository organization

**[open the root directory]** With Terragrunt we define Terraform code once, no matter how many environments we have and stop having duplicated backend code. And Allow to Define how to manage the Terraform state once in a root directory and inherit in all child modules.
**[open a module directory and terragrunt.hcl file]** It also allow to have light code per modules and fully generated backend, provider, to keep the Terraform code and the remote/provider state configuration DRY. And we can Set CLI arguments for repeatable Terraform commands once in Terragrunt configuration and run one command for all modules instead executing it in each module independently.
**[open a root directory]** As we seen the *tfstat* is not present in GitLab, but we are storing it in azure blob storage which support natively locking feature with versioning of *tfstate* files separately for each module following the infrastructure code organization (I'm using a new Azure account dedicated to this demonstration that i created using my domain name).

### tfvars encrypted

**[open .gitattributes file]** For the sensitive data such as TFVARS files we encrypt and decrypt these files with GPG keys in the background of Git commands. Once the git-crypt is properly configured, the files are encrypted on push to gitlab and decrypted when fetched from the remote server.

### .gitignore

**[open .gitignore file]** We also should have a *gitignore* file that specifies intentionally untracked files that Git should ignore. Files already tracked by Git are not affected.
So Each line in the *gitignore* file specifies a pattern. When deciding whether to ignore a path, Git normally checks *gitignore* patterns with the following order, from highest to lowest.

### .pre-commit-config

**[open .pre-commit-config file]** With pre-commit we are using git hook scripts that are useful for identifying simple issues before submission to code review. We run our hooks on every commit to automatically point out issues in code such as missing semicolons, trailing whitespace, and debug statements. By pointing these issues out before code review, this allows a code reviewer to focus on the architecture of a change while not wasting time with trivial style errors (nitpicks).

Some of these hook scripts used in the demonstration are:

* checkov:  which is a static code analysis tool for infrastructure-as-code. It scans cloud infrastructure provisioned using Terraform, Terraform plan, Cloudformation, Kubernetes, Dockerfile, Serverless or ARM Templates and detects security and compliance misconfigurations using graph-based scanning.
* tfsec  : tfsec uses static analysis of the terraform templates to spot potential security issues.
* tffmt  : to rewrite Terraform configuration files to a canonical format and style.
* tflint : to warn about deprecated syntax, unused declarations. Enforce best practices, naming conventions.
* tf-docs: to generate documentation from Terraform modules to the appropriate output formats.

### Releases

Let's take a look at the different releases of this infrastructure code **[open the release and tag tabs]**:

So we are tagging different versions and releasing new ones that can be used to deploy the infrastructure so that we can track the changes in our code by release (which is technically a pointer to a specific commit. And this pointer can be super charged with some additional information (identity of the creator of the tag, a description, a GPG signature, ...)).

* *(optional)* A tag is a git concept where a Release is a higher-level concept introduced by GitHub.

For more details on gitflow best practices take a look at this article [Git Good Practices](https://blog.yassinemaachi.com/2020/07/git-good-practices.html).

### CI/CD and environment

**[open the main repo CI/CD]** These releases are built by CI/CD pipelines running the jobs and stages for code checking and also the deployment of the release which is executed in a dedicated environment.
**[open a module repo CI/CD]** In the other hand, for the terraform modules repository we are using the GitLab Infrastructure Registry. We can use GitLab projects as a private registry for infrastructure packages and create and publish packages with GitLab CI/CD, which can then be consumed from other private projects as we saw in the main infrastructure code.

* **[open a module repo infrastructure registry]**  And when using CI/CD to build a package and push it to the infrastructure registry, we can find extended activity information when we view the package details.

### Show the issues panel

I'm also using this board to follow the issues and developments for this demonstration using my personal domain. But the best practice for the development part is to use a project management tool attached with user stories and tasks attached to in-development branches within a sprint, ...

## Local development (Prepare all the commands and actions in advance and test them)

### Development and deployment

The screen is splitted to 2 parts. The left one for the first user and the right one for the second user. In the demonstration we are going to deploy and maybe destroy the infrastructure from our local development computer which is forbidden and against the best practices, we will do it only for demonstration purpose, I highly recommend you to use CI/CD pipelines for infrastructure deployment or a dedicated tool like Atlantis:

[user-01]

```bash
cd demo/infra-dev-user-01
ll
```

[user-02]

```bash
cd demo/infra-dev-user-02
ll
```

So let's clone the infrastructure code and use a dedicated development branch for each user, and then see if we can read the files containing sensitive data:

[user-01]

```bash
git clone git@gitlab.com:t3718/public/iac-workflow.git
cd iac-workflow
git checkout feature/ephemeral-dev-user-01
```

And the same for the second user:

[user-02]

```bash
git clone git@gitlab.com:t3718/public/iac-workflow.git
cd iac-workflow
git checkout feature/persistence-dev-user-02
```

Let's see if we can read the files containing sensitive data:

[user-01] or [user-02]

```bash
cat terraform.tfvars
cat terragrunt.hcl
```

As we can see we can't read the *tfvars* file because it is encrypted. And we need also to have the key for the backend connection in order to retreive the up to date *tfstats* files.
So let get the decryption key to decrypt the *tfvars* and the backend key to connect to the terraform backend storing our *tfstats*:

First let's configure the encryption key and the access key endpoints:

[user-01] and [user-02]

```bash
export TFSTATE_BACKEND_ACCESS_KEY_FILE="https://iacworkflow.blob.core.windows.net/secrets/tfstate-backend-access-key"
export ENCRYPTION_KEY="https://iacworkflow.blob.core.windows.net/secrets/encryption-key-public"
```

For the *tfvars* we will download the encryption key and decrypt the *tfvars* using `git-crypt` command (this process can be automated so that we encrypt and decrypt these files in the background of Git commands).

[user-01] and [user-02]

```bash
curl --output ../encryption-key-public "$ENCRYPTION_KEY"
git-crypt unlock ../encryption-key-public
cat terraform.tfvars
```

For the tfstate backend access key, let's download it and inject it in our root `terragrunt.hcl` file:

[user-01] and [user-02]

```bash
curl --output ../tfstate-backend-access-key "$TFSTATE_BACKEND_ACCESS_KEY_FILE"
TFSTATE_BACKEND_ACCESS_KEY=`cat ../tfstate-backend-access-key`
sed -i '' "s|<TFSTATE_BACKEND_ACCESS_KEY>|$TFSTATE_BACKEND_ACCESS_KEY|" terragrunt.hcl
cat terragrunt.hcl
```

Now that we have all the secrets, let's see the graph dependencies in the code in order to understand the relationship between my modules without going inspecting the code manually:

[user-01] or [user-02]

```bash
terragrunt graph-dependencies
terragrunt graph-dependencies | dot -Tsvg > graph.svg
```

At this stage we can execute a `Terraform plan` using `Terragrunt CLI`. So let's run a plan at the same time from both users to see the locking funtion that will stop a parrallel execution on the infrastructure:

[user-01] and [user-02]

```bash
terragrunt run-all plan -compact-warnings
```

As we can see the first user is Acquiring a state lock per module for his execution that will be released when the execution is finished.
We are not locking the entire infrastructure but only the modules that are currently executed so that we can perform other actions on other modules non concerned by this change. in the other hand, the second user got an error while trying to acquire a state lock on the module and it fails with the following message:

```bash
│ Error: Error acquiring the state lock
│
│ Error message: state blob is already locked
│ Lock Info:
│   ID:        6d9c32f3-6889-75d4-62ea-4e69c6c11d76
│   Path:      iac-workflow-staging-tfstate/01-remote-desktop-aci/terraform.tfstate
│   Operation: OperationTypePlan
│   Who:       ymaachi001@jumpbox001
│   Version:   1.0.8
│   Created:   2021-10-14 15:04:36.538513 +0000 UTC
│   Info:
│
│
│ Terraform acquires a state lock to protect the state from being written
│ by multiple users at the same time. Please resolve the issue above and try
│ again. For most commands, you can disable locking with the "-lock=false"
│ flag, but this is not recommended.

```

That say's: there is an exesting lock of type Plan generated by this user in this host using this terraform version.
Terraform acquires a state lock to protect the state from being written by multiple users at the same time. In case you want to force unlocking of an existing terraform lock (for example a lock generated by a gitlab runner or jenkins job in a CI/CD pipeline that fails) you can use this ID to remove the lock.

Now let's run an apply from [user-01] (which is not a recommended practice) and then check from the [user-02] that the *tfstat* is up to date automatically without pulling it locally by running a plan and checking that the infrastructure is deployed:

[user-01]

```bash
terragrunt run-all apply -compact-warnings --terragrunt-non-interactive
```

**[check in azure portal and access the containers]** Let's wait for everything to be deployed on Azure.

When running a Plan from the [user-02] we should get a message telling us that the infrastructure is up to date and there is no changes. Everything in the infrastructure matches the local configuration.

[user-02]

```bash
terragrunt run-all plan -compact-warnings
```

Output message example:

```bash
Unless you have made equivalent changes to your configuration, or ignored the
relevant attributes using ignore_changes, the following plan may include
actions to undo or respond to these changes.

─────────────────────────────────────────────────────────────────────────────

No changes. Your infrastructure matches the configuration.

Your configuration already matches the changes detected above. If you'd like
to update the Terraform state to match, create and apply a refresh-only plan:
  terraform apply -refresh-only

```

If we modify the code for example by upgrading or downgrading one of the modules versions and commiting the changes. The pre-commit checks will be performed before that the code is push to git.

[user-01]

```bash
git status
git add 01-remote-desktop-aci/terragrunt.hcl
git commit -m "upgrading 01-remote-desktop-aci module to version 2.0.0"
git push

```

or

[user-02]

```bash
git status
git add 02-remote-desktop-persistence-aci/terragrunt.hcl
git commit -m "upgrading 02-remote-desktop-persistence-aci module to version 2.0.0"
git push

```

Let's take a look at the pipeline checks and create a merge request for our feature and see the artifacts

* Merge the code from `feature/persistence-dev-user-02` branch to `master` branch and run the deployment manually from gitlab (manual deploy or destroy).

One our CI/CD job is done we can try a plan from one of the users to see if the tfstat is up to date.

[user-01]

```bash
terragrunt run-all plan -compact-warnings
```

### Useful commands

* To perform module local checks with pre-commit on `azure-container-instance-persistent` module :

```bash
cd demo/iac/terraform-modules/azure-container-instance-persistent
pre-commit run -a
```

* To perform module local checks with pre-commit on `azure-container-instance-basic` module :

```bash
cd demo/iac/terraform-modules/azure-container-instance-basic
pre-commit run -a
```

* In case you want to force unlocking of an existing terraform lock (a lock generated by the gitlab runner in a CI/CD pipeline that and failes) do the following :
`cd` to the terragrunt cache directory of the locked module and run `terraform force-unlock -force <LOCK_ID>`

* To generate documentation in a module using `terraform-docs`, go to the directorie of this module and run: 

```bash
terraform-docs markdown  .  --output-file README.md
```

* To skip the pre-commit verification when you run a `git commit` command add the `--no-verify` flag:

```bash
git commit -m "upgrading module 02 to version 2.1.0" --no-verify
```